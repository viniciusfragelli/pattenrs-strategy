package Busca;

import Principal.Aluno;

public class Idade implements Busca{

	@Override
	public boolean metodoDeComparacao(Aluno var1, Aluno var2) {
		return (var1.getIdade() < var2.getIdade());
	}

}
