package Busca;

import Principal.Aluno;

public class Media implements Busca{

	@Override
	public boolean metodoDeComparacao(Aluno var1, Aluno var2) {
		return (var1.getMedia() < var2.getMedia());
	}

}
