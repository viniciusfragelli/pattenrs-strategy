package Busca;

import Principal.Aluno;

public class Nome implements Busca{

	@Override
	public boolean metodoDeComparacao(Aluno var1, Aluno var2) {
		return (var1.getNome().compareToIgnoreCase(var2.getNome()) > 0);
	}

}
