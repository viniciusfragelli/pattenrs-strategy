package Principal;

public class Aluno {
	
	private String nome;
	private int idade;
	private double media;
	
	public Aluno(String nome, int idade, double media) {
		this.nome = nome;
		this.idade = idade;
		this.media = media;
	}
	
	public int getIdade() {
		return idade;
	}
	
	public double getMedia() {
		return media;
	}
	
	public String getNome() {
		return nome;
	}
}
