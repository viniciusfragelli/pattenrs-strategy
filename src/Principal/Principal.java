package Principal;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import Busca.*;

public class Principal {
	
	private Scanner s;
	private List<Aluno> aluno;
	
	public Principal() {
		s = new Scanner(System.in);
		aluno = new ArrayList<Aluno>();
		while(true){
			System.out.println("1-Cadastrar\n"
					+ "2-Listar por...\n"
					+ "3-Sair\n"
					+ "op��o: ");
			int op = Integer.parseInt(s.next());
			switch(op){
				case 1: cadastrarAluno(); break;
				case 2: tipoDeLista(); break;
				case 3:	System.exit(1); break;
				default: continue;
			}
		}
	}
	
	private void cadastrarAluno(){
		System.out.println("Nome do aluno: ");
		String nome = s.next();
		System.out.println("Idade: ");
		int idade = s.nextInt();
		System.out.println("Media: ");
		double media = s.nextDouble();
		aluno.add(new Aluno(nome, idade, media));
	}
	
	private void tipoDeLista(){
		Spike spike = new Spike();
		ArrayList<String> lista = null;
		try {
			lista = spike.carregaCombo();
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			e.printStackTrace();
		}
		System.out.println("Ordernar por:");
		for (int i = 0; i < lista.size(); i++) {
			System.out.println(i+"-"+lista.get(i));
		}
		int op = s.nextInt();
		ordena(lista.get(op));
	}
	
	private void ordena(String tipo){
		ArrayList<Aluno> lista = new ArrayList<>(aluno);
		Class klass = null;
		Busca b = null;
		try {
			klass = Class.forName("Busca."+tipo);
			b = (Busca) klass.newInstance();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Aluno aux;
		boolean teste = true;
		while(true) {
			teste = true;
			for (int i = 0; i < lista.size()-1; i++) {
				if(b.metodoDeComparacao(lista.get(i),lista.get(i+1))){
					aux = lista.get(i+1);
					lista.set(i+1, lista.get(i));
					lista.set(i, aux);
					teste = false;
				}
			}
			if(teste)break;
		}
		
		System.out.println("Lista ordenada: ");
		
		Method[] met = Aluno.class.getMethods();
		int k = 0;
		for (int i = 0; i < met.length; i++) {
			if(met[i].getName().equalsIgnoreCase("get"+tipo)){
				k = i;
				break;
			}
		}
		
		for (int i = 0; i < lista.size(); i++) {
			try {
				if(tipo.equalsIgnoreCase("nome")){
					System.out.println(lista.get(i).getNome());
				}else{
					System.out.println(lista.get(i).getNome()+" - "+tipo+": "+met[k].invoke(lista.get(i), new Object[0]));
				}
			} catch (IllegalAccessException | IllegalArgumentException
					| InvocationTargetException e) {
				e.printStackTrace();
			}
		}
	}
		
}
