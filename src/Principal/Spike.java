package Principal;

import java.io.File;
import java.util.ArrayList;

public class Spike {

	public ArrayList<String> carregaCombo() throws InstantiationException, IllegalAccessException, ClassNotFoundException{
		ArrayList<String> aux = new ArrayList<>();
		File dir = new File("bin\\Busca");
		for(File f : dir.listFiles()){
			if(f.getName().endsWith(".class")){
				String name=f.getName().substring(0,f.getName().length()-6);
				//Class klass = Class.forName("Combo."+name);
				//if(!klass.getSimpleName().equals("Combo"))
				if(!name.equalsIgnoreCase("Busca"))
				{
					aux.add(name);
				}
			}
		}
		return aux;
	}
	
}
